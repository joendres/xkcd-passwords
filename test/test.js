const xkcdpasswords = require("../xkcdpasswords");
const expect = require('chai').expect;

describe('generate', () => {
  it('generates a 4-part english password when called without parameters', () => {
    expect(
      xkcdpasswords.generate()
    ).to.match(/^[a-z]{12,}$/u);
  });
  it('generates a 4-part english password when called with (4)', () => {
    expect(
      xkcdpasswords.generate(4)
    ).to.match(/^[a-z]{12,}$/u);
  });
  it('generates a 3-part english password when called with 3 as the first parameter', () => {
    expect(
      xkcdpasswords.generate(3)
    ).to.match(/^[a-z]{9,}$/u);
  });
  it("returns an empty string if words is negative", () => {
    expect(
      xkcdpasswords.generate(-1)
    ).to.be.empty;
  });
  it("returns an empty string if words is 0", () => {
    expect(
      xkcdpasswords.generate(0)
    ).to.be.empty;
  });
  it('separates the parts with the given joinchar', () => {
    expect(
      xkcdpasswords.generate(4, '_')
    ).to.match(/^([a-z]+_){3}[a-z]+$/u);
  });
  it("returns an empty string if joinchar is not a string", () => {
    expect(
      xkcdpasswords.generate(4, [7, 8,])
    ).to.be.empty;
  });
  it('capitalizes the parts if camel is set', () => {
    expect(
      xkcdpasswords.generate(4, '', true)
    ).to.match(/^([A-Z][a-z]+){4}$/u);
  });
  it('appends a part with char from these strings, if morechars is supplied', () => {
    expect(
      xkcdpasswords.generate(4, '', false, ["0123456789", "abcd",])
    ).to.match(/^([a-z]+){4}[0-9][a-d]$/u);
  });
  it("returns an empty string if morechars is not an array", () => {
    expect(
      xkcdpasswords.generate(4, '', false, 'wtf?')
    ).to.be.empty;
  });
  it("returns an empty string if morechars contains a not-string", () => {
    expect(
      xkcdpasswords.generate(4, '-', false, [true, 7, ["x",],])
    ).to.be.empty;
  });
  it('returns empty string if called with an empty wordlist', () => {
    expect(
      xkcdpasswords.generate(4, '', false, [], [])
    ).to.deep.equal("");
  });
  it('uses a supplied wordlist', () => {
    expect(
      xkcdpasswords.generate(4, '', false, [], ["test"])
    ).to.equal("testtesttesttest");
  });
  it("returns an empty string if wordlist is not an array", () => {
    expect(
      xkcdpasswords.generate(4, '', true, [], { "x": "0123456789", "a": "abcd", })
    ).to.be.empty;
  });
  it("returns an empty string if wordlist contains a not-string element", () => {
    expect(
      xkcdpasswords.generate(4, '', false, [], [-123, true, ["x",], { "f": "dp" },])
    ).to.be.empty;
  });
});

describe("fetch_wordlist", () => {
  it("returns an empty array if called with a non existing language code", async () => {
    expect(
      await xkcdpasswords.fetch_wordlist("xy")
    ).to.be.an('array').and.to.be.empty;
  });
  it("returns an empty array if called with a string that has more than two chars", async () => {
    expect(
      await xkcdpasswords.fetch_wordlist("xyz")
    ).to.be.an('array').and.to.be.empty;
  });
  it("returns an empty array if called with a string that contains numbers", async () => {
    expect(
      await xkcdpasswords.fetch_wordlist("42")
    ).to.be.an('array').and.to.be.empty;
  });
  it("returns a wordlist containing 'feuer' if called with language code 'de' (fails if offline)", async () => {
    expect(
      await xkcdpasswords.fetch_wordlist("de")
    ).to.include("feuer");
  });
  it("returns an empty word list if called with an invalid url", () => {
    expect(
      xkcdpasswords.fetch_wordlist("de", "http://example.com")
    ).to.be.empty;
  });
});

describe("english wordlist", () => {
  it("exists as a property", () => {
    expect(xkcdpasswords).to.have.property("_wordlist_en");
  });
  it("has 5000 words", () => {
    expect(xkcdpasswords._wordlist_en.length).to.equal(5000);
  });
  it("includes 'include'", () => {
    expect(xkcdpasswords._wordlist_en).to.include("include");
  });
});
