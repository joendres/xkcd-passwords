const fs = require('fs');
const process = require('process');
const fetch = require('node-fetch');

const all_lcs = ["af", "ar", "bg", "bn", "br", "bs", "ca", "cs", "da", "de",
    "el", "en", "eo", "es", "et", "eu", "fa", "fi", "fr", "gl", "he", "hi", "hr",
    "hu", "hy", "id", "is", "it", "ja", "ka", "kk", "ko", "lt", "lv", "mk", "ml",
    "ms", "nl", "no", "pl", "pt", "ro", "ru", "si", "sk", "sl", "sq", "sr", "sv",
    "ta", "te", "th", "tl", "tr", "uk", "ur", "vi",];

const problem_lcs = ["hy", "ur", "kk", "te", "ja", "ta", "hi", "ka", "ml", "fa", "ko", "ar",];

const pubdir = "public/";
const wordlist_dir = 'wordlists/';

const htmlhead = `<!DOCTYPE html>
    <html lang="en">
    <meta charset="UTF-8">
    <title>Available word lists</title>
    <h1>Available word lists</h1>
    <p>`;

async function make_wordlist(lc) {
    let wordlist = [];
    let response;

    try {
        response = await fetch(
            `https://raw.githubusercontent.com/hermitdave/FrequencyWords/master/content/2018/${lc}/${lc}_full.txt`);
    }
    catch (_) {
        console.error(`${lc}: network error`);
        return '';
    }
    if (response.ok) {
        let count = 4999;
        for (const line of (await response.text()).split(/\r?\n/)) {
            let word = line.split(/\s/)[0];
            if (word.match(/^\w{4,}$/u)) {
                wordlist.push(word);
                if (!(count--)) {
                    break;
                }
            }
        }
        if (wordlist.length === 5000) {
            try {
                fs.writeFileSync(pubdir + wordlist_dir + `wordlist_${lc}.json`, JSON.stringify(wordlist));
                fs.writeFileSync(pubdir + wordlist_dir + `wordlist_${lc}.js`,
                    `var xkcdpasswords_wordlist_${lc} = ` + JSON.stringify(wordlist) + ';');
                console.log(`${lc}: generated`);
                return lc;
            }
            catch (err) {
                console.log(`${lc}: file write error`);
                return '';
            }
        } else {
            console.log(`${lc}: too short`);
            return '';
        }
    }
    console.error(`${lc}: no full wordlist`);
    return '';
}

function make_index(lclist) {
    fs.writeFileSync(
        pubdir + "index.html",
        htmlhead +
        lclist
            .filter((e) => !!e)
            .sort()
            .map((e) => `<a href="${wordlist_dir}wordlist_${e}.json">${e}</a>,`)
            .join("\n"));
}

const force = process.argv.some((arg) => arg === "-f" || arg === "--force");

if (force) {
    // Do this synchronously, so we don't overlap creating new files
    const wdir = fs.opendirSync(pubdir + wordlist_dir);
    let dirent;
    while (!!(dirent = wdir.readSync())) {
        if (dirent.isFile()) {
            fs.unlinkSync(pubdir + wordlist_dir + dirent.name, () => { });
        }
    }
    wdir.close();
}

let jobs = [];
for (const lc of all_lcs) {
    if (problem_lcs.includes(lc)) {
        console.log(`${lc}: skipped`);
    } else if (force ||
        !fs.existsSync(pubdir + wordlist_dir + `wordlist_${lc}.json`) ||
        !fs.existsSync(pubdir + wordlist_dir + `wordlist_${lc}.js`)) {
        jobs.push(make_wordlist(lc));
    } else {
        console.log(`${lc}: exists`);
        jobs.push(lc);
    }
}

Promise.all(jobs)
    .then(make_index);
